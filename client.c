#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


typedef struct sockaddr_in sockaddr_in;
typedef struct sockaddr sockaddr;

//构造请求的结构体
typedef struct http_request
{
  char first_line[1024];
  char header[1000000];
  char Body[1024];
  int flag;//判断数不是POST方法，如果是将flag设为1
}http_request;


//bool JudgeFirstline(char *first_line)
//{
//  return true;
//}
//
//bool JudgeHeader(char *header)
//{
//  return true;
//}
//
//bool JudgeBody(char *body)
//{
//  return true;
//}

time_t t2;
time_t t1;

int get_line(int sock,char line[],int size)
{
  int c = 'a';
  int i = 0;
  while(i < size-1 && c != '\n')
  {
    ssize_t read_size = recv(sock,&c,1,0);
    t2 = time((time_t *)NULL);
    if(read_size > 0)
    {
      if(c == '\r')
      {
        //特殊处理将'\r' '\r\n' 都变成'\n'
        //MSG_PEEK：窥探下一个字符
        recv(sock,&c,1,MSG_PEEK); 
        if(c != '\n')
        {
          c = '\n';
        }
        else
        {
          recv(sock,&c,1,0);
        } 
      }
      line[i++] = c;
    }
    else 
    {
      break;
    }
  }
  line[i] = '\0';
  return i;
}


void *handler_request(void *arg)
{
  int64_t new_sock = (int64_t)arg;
  char line[1024] = {0};
  //得到http的响应
  //将响应的内容打出
  while(strcmp(line,"\n") != 0)
  {
    get_line(new_sock,line,sizeof(line));
    printf("%s",line); 
  }
  return (void *)0;
}


int main(int argc,char *argv[])
{
  if(argc != 2)
  {
    printf("Usage: ./client.c [url]\n");
    return 1;
  }

  //1.解析我们的url,解析出IP
  struct hostent *answer;
  char ipstr[16];//保存IP地址
  int i = 0;

  answer = gethostbyname(argv[1]);
  if(answer == NULL)
  {
    perror("gethostbyname!\n");
    return 1;
  }
  for (i = 0; (answer->h_addr_list)[i] != NULL; i++) {
  inet_ntop(AF_INET,(answer->h_addr_list)[i], ipstr, 16);

  //printf("IP:%s\n",ipstr);

  //2.创建socket进行TCP连接
  int64_t fd = socket(AF_INET,SOCK_STREAM,0);
  if(fd < 0)
  {
    return 1;
  }

  sockaddr_in addr;
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = inet_addr(ipstr);
  addr.sin_port = htons(80);
  int ret = connect(fd,(sockaddr *)&addr,sizeof(addr));
  if(ret < 0)
  {
    printf("server connect fail!\n");
    return 1;
  }

  //服务器连接成功
  printf("server connect OK!\n");

  //3.服务器连接成功了之后构造请求发送请求
  printf("首行：方法 链接 http版本\n");
  printf("例子：GET /tmp HTTP 1.1\n");
  printf("请输入首行：\n");
  http_request *request = (http_request *)malloc(sizeof(http_request));
  request->flag = 0;
  ssize_t read_size = read(0,request->first_line,sizeof(request->first_line)-1);
  if(read_size < 0)
  {
    perror("read!\n");
    return 1;
  }
  if(read_size == 0)
  {
    printf("read done\n");
    return 0;
  }
  ////判断我们所输入的首行是否正确
  request->first_line[read_size] = '\r';
  printf("\n\n");

  //输入请求头部
  printf("请求头部：属性:属性名\n");
  printf("例子：Connection: keep-alive\n");

  int i = 0;
  read_size = 0;
  printf("按enter继续输入，按q停止输入!");
  while(1)
  {
    char a = getchar();
    if(a == 'q')
    {
      break;
    }
    printf("请输入第%d行header：\n",i);
    char ret[100000] = {0};
    read_size = read(0,ret,sizeof(ret));
    if(read_size < 0)
    {
      perror("error!\n");
      return 1;
    }
    //ret[read_size] = '\n';
    strcat(request->header,ret);
    //printf("按enter继续输入，按q停止输入!");
    i++;
  }
   //printf("\n%s\n",request->header);
   
  if(read_size < 0)
  {
    perror("read!\n");
    return -1;
  }
  if(read_size == 0)
  {
    printf("read done！\n");
    return 0;
  }
  request->header[read_size] = '\n';

  //POST方法的请求有正文
  //GET请求的方法没正文
  //此时就要解析我们输入的首行判断是POST方法还是GET方法
  //GET方法就不需要输入正文
  //POST就输入正文
  char *begin = request->first_line;
  char method[10] = {0};
  for(i = 0;*begin != ' ';i++,begin++)
  {
    method[i] = *begin;
  }
  method[i] = '\0';
  if(strcmp(method,"POST") != 0)
  {
    request->header[read_size] = '\0'; 
  }


  //处理POST情况
  if(strcmp(method,"POST") == 0)
  {
    request->flag = 1;
    printf("正文：键值对\n");
    printf("例子：");
    printf("请输入body：");
    printf("按enter继续输入，按q停止输入!");
    int i = 0;
    while(getchar() != 'q')
    {
      printf("请输入第%d行header：\n",i);
      char ret[10000] = {0};
      read_size = read(0,ret,sizeof(ret));
      if(read_size < 0)
      {
        perror("perror!\n");
        return -1;
      }
      ret[read_size] = '\n';
      strcat(request->Body,ret);
      //printf("按enter继续输入，按q停止输入!");
      i++;
    }
    if(read_size < 0)
    {
      perror("read\n");
      return -1;
    }
    if(read_size == 0)
    {
      printf("read done!\n");
      return 0;
    }
    request->Body[read_size] = '\0';
  }
  printf("structure request OK!\n");
  //b)构造报文
  //根据有无POST方法
  char *httprequest;
  if(request->flag == 0)
  {
    httprequest = strcat(request->first_line,request->header);
  }
  else
  {
    char *ret = strcat(request->first_line,request->header);
    httprequest = strcat(ret,request->Body); 
  }

  //printf("first:\n%s",request->first_line);
  //printf("header:\n%s",request->header);

  printf("\nrequest:\n%s\n",httprequest);
  //将报文写到socket里

  write(fd,httprequest,sizeof(httprequest));
  char response[1000] = {0};
  get_line(fd,response,sizeof(response));
  
  t1 = time((time_t *)NULL);
  return 0;
   }  
}
